Rails.application.routes.draw do
  resources :customers do
    collection do
      get :authenticate
      get :oauth_callback
    end
  end

  root to: 'customers#index'
end
